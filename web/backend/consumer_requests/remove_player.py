import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.utils.enums import RequestTypeEnum
from backend.utils.game_manager import del_prop_deleting_game


class RemovePlayer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        pass

    @staticmethod
    async def respond(consumer, event):
        del_prop_deleting_game(consumer.game_id)

        name = event['name']

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REMOVE_PLAYER.value,
            'name': name
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True

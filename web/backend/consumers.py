import asyncio
import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from backend.consumer_requests.get_players import GetPlayers
from backend.consumer_requests.register_player import RegisterPlayer
from backend.consumer_requests.remove_player import RemovePlayer
from backend.consumer_requests.reset import Reset
from backend.utils.enums import RequestTypeEnum
from backend.utils.game_manager import set_prop_deleting_game, del_prop_deleting_game, start_game_deletion_delay
from backend.utils.player_manager import player_exists, remove_player


class BackendConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.game_id = self.scope['url_route']['kwargs']['game_id']

        del_prop_deleting_game(self.game_id)

        self.game_group_name = 'game_%s' % self.game_id

        self.player_name = ''
        self.white_team = -1

        # Join room group
        await self.channel_layer.group_add(
            self.game_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, code):
        # Remove player from database if it has been registered
        if await database_sync_to_async(player_exists)(self.game_id, self.player_name):
            await database_sync_to_async(remove_player)(self.game_id, self.player_name)

        set_prop_deleting_game(self.game_id)

        # Send message to room group
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REMOVE_PLAYER.value,
                'name': self.player_name
            }
        )

        # Leave room group
        await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )

        asyncio.create_task(start_game_deletion_delay(self.game_id))

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        request_type = text_data_json['type']
        if not self.validate_call(text_data_json):
            await self.send(text_data=json.dumps({
                'type': request_type,
                'error': 1
            }))
            return
        if request_type == RequestTypeEnum.REGISTER_PLAYER.value:
            await RegisterPlayer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.REMOVE_PLAYER.value:
            await RemovePlayer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_PLAYERS.value:
            await GetPlayers.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.RESET.value:
            await Reset.treat_call(self, text_data_json)

    async def respond(self, event):
        res_type = event['res_type']
        if res_type == RequestTypeEnum.REGISTER_PLAYER.value:
            await RegisterPlayer.respond(self, event)
        elif res_type == RequestTypeEnum.REMOVE_PLAYER.value:
            await RemovePlayer.respond(self, event)
        elif res_type == RequestTypeEnum.GET_PLAYERS.value:
            await GetPlayers.respond(self, event)
        elif res_type == RequestTypeEnum.RESET.value:
            await Reset.respond(self, event)

    async def validate_call(self, text_data_json):
        request_type = text_data_json['type']
        if request_type == RequestTypeEnum.REGISTER_PLAYER.value:
            await RegisterPlayer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.REMOVE_PLAYER.value:
            await RemovePlayer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_PLAYERS.value:
            await GetPlayers.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.RESET.value:
            await Reset.valid_call(self, text_data_json)

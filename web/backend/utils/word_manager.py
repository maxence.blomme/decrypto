from backend.models import GameWords
from backend.seralizers import WhiteWordsSerializer, BlackWordsSerializer


def get_words(game_id, white=True, serialized=False):
    words = GameWords.objects.filter(game_id=game_id)[0]
    if serialized:
        if white:
            return WhiteWordsSerializer(words).data
        else:
            return BlackWordsSerializer(words).data
    else:
        return words

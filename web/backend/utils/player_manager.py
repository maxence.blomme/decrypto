from backend.models import Player
from backend.seralizers import PlayerSerializer


def player_exists(game_id, player_name):
    player = Player.objects.filter(game_id=game_id, name=player_name)
    return len(player) > 0


def add_player(game_id, player_name, white_team):
    Player.objects.create(game_id=game_id, name=player_name, white_team=white_team)


def remove_player(game_id, player_name):
    Player.objects.filter(game_id=game_id, name=player_name).delete()


def delete_players(game_id):
    Player.objects.filter(game_id=game_id).delete()


def toggle_spy(game_id, player_name, is_spy):
    Player.objects.filter(game_id=game_id, name=player_name).update(is_spy=is_spy)


def get_players(game_id, serialized=False):
    players = Player.objects.filter(game_id=game_id)
    if serialized:
        return PlayerSerializer(players, many=True).data
    else:
        return players


def get_player(game_id, player_name, serialized=False):
    player = Player.objects.filter(game_id=game_id, name=player_name)[0]
    if serialized:
        return PlayerSerializer(player).data
    else:
        return player

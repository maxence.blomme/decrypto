import asyncio
import re

from asgiref.sync import sync_to_async

from backend.models import Game, GameWords, Clue, Player
from backend.seralizers import GameSerializer
from backend.utils.redis import Redis


def valid_game_id(game_id):
    return 0 < len(game_id) <= 32 and re.match(r"^[a-zA-Z0-9]+$", game_id)


def game_exists(game_id):
    return len(Game.objects.filter(game_id=game_id)) > 0


def get_game(game_id, serialized=False):
    game = Game.objects.filter(game_id=game_id)[0]
    if serialized:
        return GameSerializer(game).data
    else:
        return game


def is_private(game_id):
    return Game.objects.filter(game_id=game_id)[0].private


def clean_game(game_id):
    delete_game(game_id)
    Player.objects.filter(game_id=game_id).delete()


def delete_game(game_id):
    Game.objects.filter(game_id=game_id).delete()
    GameWords.objects.filter(game_id=game_id).delete()
    Clue.objects.filter(game_id=game_id).delete()


def set_prop_deleting_game(game_id):
    Redis().set('deleting-' + game_id, 1)


def get_prop_deleting_game(game_id):
    Redis().get('deleting-' + game_id)


def del_prop_deleting_game(game_id):
    Redis().delete('deleting-' + game_id)


async def start_game_deletion_delay(game_id):
    await asyncio.sleep(10)
    if not get_prop_deleting_game(game_id):
        return
    await asyncio.sleep(600)
    if get_prop_deleting_game(game_id):
        await sync_to_async(clean_game)(game_id)

from enum import Enum


class RequestTypeEnum(Enum):
    REGISTER_PLAYER = 0
    REMOVE_PLAYER = 1
    GET_PLAYERS = 2
    RESET = 3

from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import mixins, status

from backend.seralizers import GameCreationSerializer
from backend.utils.game_creation import generate_game
from backend.utils.game_manager import valid_game_id, game_exists


class CreateGame(mixins.CreateModelMixin,
                       GenericAPIView):
        game_id = ''
        serializer_class = GameCreationSerializer

        def post(self, request, *args, **kwargs):
            game_id = request.data.get('game_id', '')
            private = request.data.get('private', False)
            private = False if private == '' else bool(private)

            if not valid_game_id(game_id):
                return Response(status=status.HTTP_400_BAD_REQUEST)
            elif game_exists(game_id):
                return Response(status=status.HTTP_200_OK)

            generate_game(game_id, private)

            return Response(status=status.HTTP_201_CREATED)
